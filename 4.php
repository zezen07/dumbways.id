<!DOCTYPE html>
<html>
	<head>
		<title>Crud Sederhana</title>
	</head>
	<body>
<?php
	// --- koneksi ke database
	$koneksi = mysqli_connect("localhost","root","","library") or die(mysqli_error());
	// --- Fngsi tambah data (Create)
	function tambah($koneksi){
	    
	    if (isset($_POST['btn_simpan'])){
	        $id = mysqli_insert_id($koneksi);
	        $book_name = $_POST['book_name'];
	        $publication = $_POST['publication'];
	        $upload = $_POST['upload'];
	        $category_id = $_POST['category_id'];
	        $writer_id = $_POST['writer_id'];
	        
	        if(!empty($book_name) && !empty($publication) && !empty($upload) && !empty($category_id) && !empty($writer_id)){
	           
	            $sql = "INSERT INTO book_tb (id, name, publication_year, img, category_id, writer_id) VALUES('".$id."','".$book_name."','".$publication."','".$upload."','".$category_id.",'".$writer_id."'')";
	            $simpan = mysqli_query($koneksi, $sql);
	            
	            if($simpan && isset($_GET['aksi'])){
	                if($_GET['aksi'] == 'create'){
	                    header('location: 4.php');
	                }
	            }

	        } else {
	            $pesan = "Tidak dapat menyimpan, data belum lengkap!";
	        }
	    }
	// }//end function
?>
		<form action="" method="POST">
			<fieldset>
				<h2>Tambah Data</h2>
				<label>Nama Buku    :<input type="text" name="book_name" /></label> <br>
				<label>Tahun Terbit :<input type="text" name="publication" /></label> <br>
				<label>Upload Gambar:<input type="text" name="upload" /></label><br>
				<label>Kategori     :
					<select name="category_id">
						<option value="">----Pilih Kategori----</option>
						<?php
							$sqlCategory= "SELECT * FROM category_tb";
							$queryCategory= mysqli_query($koneksi, $sqlCategory);
							while($dataCategory = mysqli_fetch_array($queryCategory)){
								echo "<option value='".$dataCategory['id']."'>".$dataCategory['name']."</option>";
							}
						?>
					</select>
				</label><br>
				<label>Penulis      :
					<select name="writer_id">
						<option value="">----Pilih Penulis----</option>
						<?php
							$sqlWriter= "SELECT * FROM writer_tb";
							$queryWriter= mysqli_query($koneksi, $sqlWriter);
							while($dataWriter = mysqli_fetch_array($queryWriter)){
								echo "<option value='".$dataWriter['id']."'>".$dataWriter['name']."</option>";
							}
						?>
					</select>
				</label><br>
				<label>
					<input type="submit" name="btn_simpan" value="Simpan"/>
                    <input type="reset" name="reset" value="Bersihkan"/>
				</label>
				<br>
               <p><?php echo isset($pesan) ? $pesan : "" ?></p>	
			</fieldset>
		</form>
<?php  
	}// --- Tutup Fngsi tambah data
	// --- Fungsi Baca Data (Read)
	function tampil_data($koneksi){
	    $sql = "SELECT * FROM book_tb join category_tb on book_tb.category_id = category_tb.id join writer_tb on book_tb.writer_id = writer_tb.id";
	    $query = mysqli_query($koneksi, $sql);
	    
	    echo "<fieldset>";
	    echo "<legend><h2>Data Buku</h2></legend>";
	    
	    echo "<table border='1' cellpadding='10'>";
	    echo "<tr>
	            <th>id</th>
	            <th>Nama Buku</th>
	            <th>publication</th>
	            <th>Gambar</th>
	            <th>Category</th>
	            <th>Writer</th>
	          </tr>";
	    while($data = mysqli_fetch_array($query)){
	    	return $data;
	    	$no++;
?>
	            <tr>
	                <td><?php echo $no; ?></td>
	                <td><?php echo $data['name']; ?></td>
	                <td><?php echo $data['publication_year']; ?></td>
	                <td><?php echo $data['img']; ?></td>
	                <td><?php echo $data['category_tb']['name']; ?></td>
	                <td><?php echo $data['writer_tb.name']; ?></td>
	                <td>
	                    <a 	href="4.php?aksi=update&id=<?php echo $data['id']; ?>&name=<?php echo $data['name']; ?>&publication=<?php echo $data['publication_year']; ?>&upload=<?php echo $data['img']; ?>&category_id=<?php echo $data['category_id']; ?>&writer_id=<?php echo $data['writer_id']; ?>">Ubah
	                	</a> |
	                    <a href="4.php?aksi=delete&id=<?php echo $data['id']; ?>">Hapus</a>
	                </td>
	            </tr>
<?php
	    }
	    echo "</table>";
	    echo "</fieldset>";
	}
// --- Tutup Fungsi Baca Data (Read)
// --- Fungsi Ubah Data (Update)
function ubah($koneksi){
    // ubah data
    if(isset($_POST['btn_ubah'])){
        $id = $_POST['id'];
        $book_name = $_POST['book_name'];
        $publication = $_POST['publication'];
        $upload = $_POST['upload'];
        $category_id = $_POST['category_id'];
        $writer_id = $_POST['writer_id'];
        
        if(!empty($book_name) && !empty($publication) && !empty($upload) && !empty($category_id) && !empty($writer_id)){
            $perubahan = "name'".$book_name."',publication_year=".$publication.",img=".$upload.",category_id='".$category_id."',writer_id='".$writer_id."'";
            $sql_update = "UPDATE book_tb SET ".$perubahan." WHERE id=$id";
            $update = mysqli_query($koneksi, $sql_update);
            if($update && isset($_GET['aksi'])){
                if($_GET['aksi'] == 'update'){
                    header('location: 4.php');
                }
            }
        } else {
            $pesan = "Data tidak lengkap!";
        }
    }
    
    // tampilkan form ubah
    if(isset($_GET['id'])){
?>
            <a href="4.php"> &laquo; Home</a> | 
            <a href="4.php?aksi=create"> (+) Tambah Data</a>
            <hr>
            
            <form action="" method="POST">
				<fieldset>
					<h2>Tambah Data</h2>
					<label>Nama Buku    :<input type="text" name="book_name" /></label> <br>
						<label>Tahun Terbit :<input type="text" name="publication" /></label> <br>
						<label>Upload Gambar:<input type="text" name="upload" /></label><br>
						<label>Kategori     :
							<select name="category_id">
								<option value="">----Pilih Kategori----</option>
								<?php
									$sqlCategory= "SELECT * FROM category_tb";
									$queryCategory= mysqli_query($koneksi, $sqlCategory);
									while($dataCategory = mysqli_fetch_array($queryCategory)){
										echo "<option value='".$dataCategory['id']."'>".$dataCategory['name']."</option>";
									}
								?>
							</select>
						</label><br>
						<label>Penulis      :
							<select name="writer_id">
								<option value="">----Pilih Penulis----</option>
								<?php
									$sqlWriter= "SELECT * FROM writer_tb";
									$queryWriter= mysqli_query($koneksi, $sqlWriter);
									while($dataWriter = mysqli_fetch_array($queryWriter)){
										echo "<option value='".$dataWriter['id']."'>".$dataWriter['name']."</option>";
									}
								?>
							</select>
						</label><br>
						<label>
						<input type="submit" name="btn_ubah" value="Simpan"/>atau 
						<a 
							href="4.php?aksi=delete&id=<?php echo $_GET['id'] ?>"> (x) Hapus data ini
						</a>!
					</label>
					<br>
	               <p><?php echo isset($pesan) ? $pesan : "" ?></p>	
				</fieldset>
			</form>
<?php
    }
    
}
// --- Tutup Fungsi Update
// --- Fungsi Delete
function hapus($koneksi){
    if(isset($_GET['id']) && isset($_GET['aksi'])){
        $id = $_GET['id'];
        $sql_hapus = "DELETE FROM tabel_panen WHERE id=" . $id;
        $hapus = mysqli_query($koneksi, $sql_hapus);
        
        if($hapus){
            if($_GET['aksi'] == 'delete'){
                header('location: 4.php');
            }
        }
    }
    
}
// --- Tutup Fungsi Hapus
	// --- Program Utama
	if (isset($_GET['aksi'])){
	    switch($_GET['aksi']){
	        case "create":
	            echo '<a href="4.php"> &laquo; Home</a>';
	            tambah($koneksi);
	            tampil_data($koneksi);
	            break;
	        case "read":
	            tampil_data($koneksi);
	            break;
	        case "update":
	            ubah($koneksi);
	            tampil_data($koneksi);
	            break;
	        case "delete":
	            hapus($koneksi);
	            break;
	        default:
	            echo "<h3>Aksi <i>".$_GET['aksi']."</i> tidak ada!</h3>";
	            tambah($koneksi);
	            tampil_data($koneksi);
	    }
	} else {
	    tambah($koneksi);
	    tampil_data($koneksi);
	}
?>
	</body>
</html>